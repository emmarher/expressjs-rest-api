


## **Descripcción**

Proyecto desarrollado en _NODE.JS_, utilizando el framework minimalista _EXPRESS.JS_, para desarrollar una pequeña app que conecta con diferentes rutas, conectando a su vez via api a una ruta externa convirtiendo y mostrando los valores en JSON.

Posteriormente hacer pruebas unitarias utilizando JEST y pruebas de API (get,post, delete, etc) utilizando supertest.

Es muy importante tener en cuanta las versiones de los modulos y el node, ya que esto difiere en la sintaxis del código, y es posible que llegue a generar errores.



En el package.json podemos hacer la configuracion de inicio, creamos el comando _dev_ y previamente instalado nodemon, se configura (vease el archivo package.json).
Ahora solo tenemos que llamar el comando creado para ejectuarlo.
~~~
npm run dev
~~~
y como esta llamando a nodemon, se estará actualizando de manera automatica en los cambios del codigo.

Supertest se encarga de realizar peticiones a las rutas, mientras que jest hace la prueba unitaria, por lo tanto, utilizamos jest para probar las rutas mediante supertest.

**Estructura del proyecto**

Archivos dentro del proyecto.
~~~
EXPRESSJS-REST-API
|--- README.md
|--- package.json
|--- package-lock.json   
└─── node_modules
|   |--- (contiene todo los archivos necesarios para node.js)
└─── src
|   |--- app.js
|   |--- index.js
|   |--- sample.json
|   └─── routes
|       |--- index.js
|       |--- movies.js
|       |--- users.js
|   └─── test
|       |--- index.spec.js


~~~


Listar los paquetes instalados

- npm ls

Para manejar arreglos y procesarlos

- npm instal underscore


Para hacer peticiones get, post, delete en otros servicios

- npm install node-fetch



----
## **Pruebas unitarias**

Todos los archivos que son creados para las pruebas unitarias se recomienda que esten agrupados dentro de una carpeta con el nombre test, en adición, despues del nombre del archivo, la primera extención es _spec_, seguido de la extención del lenguaje, en este caso al ser ExpressJS (JavaScript) se agrega _js_; ejemplo:
*index.spec.js*.


### **Supertest modulo para hacer las peticiones al backend**

La "-D" es utilizado par que se agregue como modulo de desarrollo (develop) y no es requerido para su funcionamiento normal

- npm install -D jest supertest
- JEST framework de testing


---
### **Configurar modulos de jest**

NODE_OPTIONS=--experimental-vm-modules npx jest

NODE_OPTIONS=--experimental-vm-modules => Esto espara configurar los modulos, posteriormente basta con solo ejectuar npx jest.

Tambien funciona NODE_OPTIONS=--experimental-vm-modules && jest

En caso de hacerlo en Windows OS, al inicio se utiliza el comando set NODE_...


Dependiendo de la version de node (aun desconozco como es el funcionamiento interno de la disparidad de versiones con los modulos), en algunos se permite utilzar el import y en otros es requerido importar usando require.

_NOTA: se recomienda crear una carpeta para los script de test._

Las diferentes formas de importar dependiendo de la versión:
Importar un modulo en otra ruta



~~~
//Realiza lo mismo pero de forma diferente
import moduleName from '../src/app';
const app = require('../src/app.js');

//Realiza lo mismo pero de forma diferente
importar un modulo de node
const request = require('supertest');
import request from 'supertest';

~~~
Se tiene que tener en cuenta que los test creados deben poder  comunicarse sin problema con los modulos y funciones del app, si se espera que en la app cuando se envia cierto objeto con una forma definida, con base en ello, se tiene que desarrollar la prueba para el mismo.