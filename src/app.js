const express = require('express');
//import express from 'express';
const app = express();
const morgan = require('morgan');

//settings
app.set('port',process.env.PORT || 3000); //en caso existe un puerto definido por el servicio, si no usa el 3000
app.set('json spaces', 2);


//middlewares
app.use(morgan('combined'));
app.use(express.urlencoded({extended: false})); //enteder datos input del formulario, soportar datos a recibir
app.use(express.json());


//Routes
app.use(require('./routes/index'));
app.use('/api/movies', require('./routes/movies'));
app.use('/api/users', require('./routes/users'));


//export {app};
module.exports = app;