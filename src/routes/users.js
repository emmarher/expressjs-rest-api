const { Router } = require('express');
const router = Router();

//const fetch = require('node-fetch');
// se importa de la siguiente forma, ya que es la ver 3. y no necesita usar require
const fetch = (...args) => import('node-fetch').then(({default: fetch}) => fetch(...args));

router.get('/', async (req, res) => {
    //Petición asincrona, toma tiempo para obetner los datos, por eso ponemos el metodo async await
    const response = await fetch('https://jsonplaceholder.typicode.com/users'); // se recibe como string
    const users = await response.json();
    console.log(users);
    res.json(users);
});


module.exports = router;