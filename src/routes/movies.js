const { Router} = require('express');
const router = Router();
const _ = require('underscore');

const movies = require('../sample.json');


router.get('/',(req,res) => {
    res.json(movies);
});


router.post('/', (req, res) => {
    
    const {title, director, year, rating } = req.body; // se guardan los valores de json en esas constantes
    if (title && director && year && rating){
        
        const id = movies.length + 1;
        const newMovie = {...req.body, id};
        movies.push(newMovie);
        res.json(movies);
        
        //console.log(req.body);
    } else{
        res.send('Hubo un error');
    }
    
    
});

router.delete('/:id', (req, res) => {
    const { id } = req.params;    //const {} = req.params
    //cada vez que recorra el arreglo, obtiene una funcion, recibe una sola pelicula y un indice
    _.each(movies, (movie, i ) => {
        //si el id de movie es igual al id que recibe
        if (movie.id == id) {
            //utilizamos el metodo splice para remover
            movies.splice(i,1);
        }
    });
    res.send(movies);
});


router.put('/:id',(req,res) => {
    const {id} = req.params;
    const {title, director, year, rating } = req.body;
    if (title && director && year && rating){
        _.each(movies, (movie, i) =>{
            if (movie.id == id){
                movie.title = title;
                movie.director = director;
                movie.year = year;
                movie.rating = rating;
            }
            
        });
        res.json(movies);
    } else {
        res.status(500).json({error: 'Hubo un error'});
    }
});






module.exports = router;