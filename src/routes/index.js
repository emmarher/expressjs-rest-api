const { Router} = require('express'); //desde express requiero el metodo router
const { route } = require('express/lib/application');
// permite definir nuevas rutas
const router = Router();

router.get('/',(req,res) => {
    res.json({"Title": "Hola k hace"});
});

router.get('/test',(req,res) => {
    const data = {
        'name': 'Ali',
        'Website': 'aliz.net'
    }
    res.json(data);
});

module.exports = router;