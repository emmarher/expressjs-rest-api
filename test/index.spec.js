//import moduleName from '../src/app';
const app = require('../src/app.js');
const request = require('supertest');
//import request from 'supertest';
//const request = supertest.request();


describe('describe de prueba de get ', () =>{

    test('should respond with a 200 status code url: /movies', async () =>{
        const response = await request(app).get('/movies').send();//peticion asincrona con request a la app, el send hace como un envio (enter)
        //console.log(response);
        expect(response.statusCode).toBe(200);
    });


    test('should respond with a 200 status code url: /api/movies', async () =>{
        const response = await request(app).get('/api/movies').send();//peticion asincrona con request a la app, el send hace como un envio (enter)
        //console.log(response);
        expect(response.statusCode).toBe(200);
    });

    test('should respond with arrays url: /api/movies', async () =>{
        const response = await request(app).get('/api/movies').send();//peticion asincrona con request a la app, el send hace como un envio (enter)
        //console.log(response);
        expect(response.body).toBeInstanceOf(Array);
        //console.log(response.body);
    });
    
});

describe('POST /api/movies', () => {

    const objeto = {
        "title" : "Aliens2",
        "director":"James mchaer",
        "year": "1986",
        "rating":"8.5"
    }; 

    test('Should respond with status code 200', async () => {
        const response = await request(app).post('/api/movies').send();
        expect(response.statusCode).toBe(200);
    });

    test('Should have a content-type application/json in header', async() =>{ 

        
        const response = await request(app).post('/api/movies').send(objeto);
        expect(response.header['content-type']).toEqual(expect.stringContaining('json'));

    });

    test(' Should have ID defined', async () => {

        // Utiliza el mismo objeto que el test anterior, por lo que los valores se repiten, exceptuando el id que es en incremental automatico
        const response = await request(app).post('/api/movies').send(objeto);
        //console.log('ID defined?');
        //console.log('header',response.header);
        //console.log('body id: ',response._body[0]);
        //console.log('StatusCode:   ',response.statusCode);
        //console.log(response);

        expect(response._body[0]).toHaveProperty('id');
        

    });

});

describe ('No existe el titulo', () =>{

    const objeto2 = {
        "title" : "No mires arriba",
        "year": "2021"
    };

    test('Should respond with 400 status code if doesnt exists some fields', async () =>{
        const response = await request(app).post('/api/movies').send(objeto2);
        console.log(response);
        expect(response.text).toBe('Hubo un error');
    });
});